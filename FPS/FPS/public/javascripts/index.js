﻿'use strict';

var authUrl = 'http://localhost:9001/api';
var customerUrl = 'http://localhost:9002/api';
var supplierUrl = 'http://localhost:9000/api';
var productionUrl = 'http://localhost:9003/api';

var sessionObject = {};
var sessionJWT = {};
var indexViewModel = ko.observable(null);

function IndexViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);

    if (sessionObject.role.slug == 'admin') {
        $("#navHeader").load("/adminHeader");
        self.isAdmin(true);

        self.totalKgsPerYarn = ko.observableArray([]);
        self.totalSalesPerYarn = ko.observableArray([]);
        self.totalClosedSales = ko.observableArray([]);

        self.productionSales = ko.observableArray([]);
        self.salesOrders = ko.observableArray([]);
        self.yarns = ko.observableArray([]);
        self.fabrics = ko.observableArray([]);

        $.ajax({
            type: 'GET',
            url: productionUrl + '/fabric',
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                var i = 0;
                for (i = 0; i < data.length; i++) {
                    self.fabrics.push({
                        id: data[i].id,
                        name: data[i].name,
                        composition: data[i].composition,
                        description: data[i].description,
                        yarns: data[i].FabricYarns
                    });
                }

                $.ajax({
                    type: 'GET',
                    url: productionUrl + '/production',
                    contentType: "application/json",
                    headers: { 'jwt': sessionJWT },
                    success: function (data) {
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            self.productionSales.push({
                                id: data[i].id,
                                sales_order_id: data[i].sales_order_id,
                                fabric_id: data[i].fabric_id,
                                quantity: data[i].quantity,
                                created_at: data[i].created_at
                            });
                        }

                        for (i = 0; i < self.productionSales().length; i++) {
                            var j = 0;
                            for (j = 0; j < self.fabrics().length; j++) {
                                if (self.productionSales()[i].fabric_id === self.fabrics()[j].id) {
                                    var x = 0;
                                    for (x = 0; x < self.fabrics()[j].yarns.length; x++) {
                                        var aux = 0;
                                        if (self.totalKgsPerYarn().length === 0) {
                                            self.totalKgsPerYarn.push({
                                                id: self.fabrics()[j].yarns[x].Yarn.id,
                                                label: self.fabrics()[j].yarns[x].Yarn.code + " - " + self.fabrics()[j].yarns[x].Yarn.name,
                                                y: self.productionSales()[i].quantity * self.fabrics()[j].yarns[x].yarn_percentage / 100
                                            });
                                        } else {
                                            var y = 0;
                                            var check = false;
                                            for (y = 0; y < self.totalKgsPerYarn().length; y++) {
                                                if (self.totalKgsPerYarn()[y].id === self.fabrics()[j].yarns[x].Yarn.id) {
                                                    self.totalKgsPerYarn()[y].y += self.productionSales()[i].quantity * self.fabrics()[j].yarns[x].yarn_percentage / 100;
                                                    check = true;
                                                    break;
                                                }
                                            }

                                            if (!check) {
                                                self.totalKgsPerYarn.push({
                                                    id: self.fabrics()[j].yarns[x].Yarn.id,
                                                    label: self.fabrics()[j].yarns[x].Yarn.code + " - " + self.fabrics()[j].yarns[x].Yarn.name,
                                                    y: self.productionSales()[i].quantity * self.fabrics()[j].yarns[x].yarn_percentage / 100
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        for (i = 0; i < self.productionSales().length; i++) {
                            if (data[i].status_id === 3) { // YOU CAN CHANGE THIS
                                var j = 0;
                                for (j = 0; j < self.fabrics().length; j++) {
                                    if (self.productionSales()[i].fabric_id === self.fabrics()[j].id) {
                                        var x = 0;
                                        for (x = 0; x < self.fabrics()[j].yarns.length; x++) {
                                            var aux = 0;
                                            if (self.totalSalesPerYarn().length === 0) {
                                                self.totalSalesPerYarn.push({
                                                    id: self.fabrics()[j].yarns[x].Yarn.id,
                                                    label: self.fabrics()[j].yarns[x].Yarn.code + " - " + self.fabrics()[j].yarns[x].Yarn.name,
                                                    y: 1
                                                });
                                            } else {
                                                var y = 0;
                                                var check = false;
                                                for (y = 0; y < self.totalSalesPerYarn().length; y++) {
                                                    if (self.totalSalesPerYarn()[y].id === self.fabrics()[j].yarns[x].Yarn.id) {
                                                        self.totalSalesPerYarn()[y].y++;
                                                        check = true;
                                                        break;
                                                    }
                                                }

                                                if (!check) {
                                                    self.totalSalesPerYarn.push({
                                                        id: self.fabrics()[j].yarns[x].Yarn.id,
                                                        label: self.fabrics()[j].yarns[x].Yarn.code + " - " + self.fabrics()[j].yarns[x].Yarn.name,
                                                        y: 1
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (self.totalKgsPerYarn().length > 4) {
                            self.totalKgsPerYarn().sort((a, b) => (a.y > b.y) ? -1 : ((b.y > a.y) ? 1 : 0));
                            var d = self.totalKgsPerYarn().length;
                            var auxCount = d;
                            for (d = auxCount; d > 4; d--) {
                                self.totalKgsPerYarn().pop();
                            }
                        }

                        $("#chartTotalsYarnKgs").CanvasJSChart({
                            title: {
                                text: "Total of kgs per yarn"
                            },
                            subtitles: [{
                                text: "Since ever"
                            }],
                            animationEnabled: true,
                            data: [{
                                type: "column",
                                toolTipContent: "<b>{label}</b>: {y} kgs",
                                showInLegend: "true",
                                legendText: "{label}",
                                dataPoints: self.totalKgsPerYarn()
                            }]
                        });
                        
                        if (self.totalSalesPerYarn().length > 4) {
                            self.totalSalesPerYarn().sort((a, b) => (a.y > b.y) ? -1 : ((b.y > a.y) ? 1 : 0));
                            var d = self.totalSalesPerYarn().length;
                            var auxCount = d;
                            for (d = auxCount; d > 4; d--) {
                                self.totalSalesPerYarn().pop();
                            }
                        }

                        $("#pieTotalsYarnSales").CanvasJSChart({
                            title: {
                                text: "Total of sales per yarn"
                            },
                            subtitles: [{
                                text: "Since ever"
                            }],
                            animationEnabled: true,
                            data: [{
                                type: "pie",
                                startAngle: 40,
                                toolTipContent: "<b>{y} sales",
                                showInLegend: "true",
                                legendText: "{label}",
                                indexLabelFontSize: 16,
                                indexLabel: "{label}",
                                dataPoints: self.totalSalesPerYarn()
                            }]
                        });
                    }
                });
            }
        });

        $.ajax({
            type: 'GET',
            url: customerUrl + '/so',
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                var i = 0;
                for (i = 0; i < data.length; i++) {
                    self.salesOrders.push({
                        id: data[i].id,
                        customer_id: data[i].customer_id,
                        fabric_id: data[i].fabric_id,
                        fabric: data[i].fabric,
                        quantity: data[i].quantity,
                        color_code_id: data[i].color_code_id,
                        color_code: data[i].color_code,
                        print_code_id: data[i].print_code_id,
                        print_code: data[i].print_code,
                        delivery_date: data[i].delivery_date,
                        delivery_address: data[i].delivery_address,
                        delivery_zip: data[i].delivery_zip,
                        delivery_city: data[i].delivery_city,
                        delivery_country: data[i].delivery_country,
                        status_id: data[i].status_id,
                        created_at: data[i].created_at
                    });
                }

                var auxValidation = new Date('2018-01-01');
                for (i = 0; i < self.salesOrders().length; i++) {
                    if (data[i].status_id === 3) { // YOU CAN CHANGE THIS
                        var auxCreated = new Date(data[i].created_at);
                        if (auxValidation => auxCreated) {
                            if (self.totalClosedSales().length === 0) {
                                self.totalClosedSales.push({
                                    label: auxCreated.toDateString(),
                                    y: 1
                                });
                            } else {
                                var y = 0;
                                var check = false;
                                for (y = 0; y < self.totalClosedSales().length; y++) {
                                    if (self.totalClosedSales()[y].label === auxCreated.toDateString()) {
                                        self.totalClosedSales()[y].y++;
                                        check = true;
                                        break;
                                    }
                                }

                                if (!check) {
                                    self.totalClosedSales.push({
                                        label: auxCreated.toDateString(),
                                        y: 1
                                    });
                                }
                            }
                        }
                    }
                }

                if (self.totalClosedSales().length > 4) {
                    self.totalClosedSales().sort((a, b) => (a.y > b.y) ? -1 : ((b.y > a.y) ? 1 : 0));
                    var d = self.totalClosedSales().length;
                    var auxCount = d;
                    for (d = auxCount; d > 4; d--) {
                        self.totalClosedSales().pop();
                    }
                }

                $("#chartTotalsClosedSales").CanvasJSChart({
                    title: {
                        text: "Total of closed sales orders"
                    },
                    subtitles: [{
                        text: "Since January 1st"
                    }],
                    animationEnabled: true,
                    data: [{
                        type: "column",
                        toolTipContent: "<b>{label}</b>: {y} orders",
                        showInLegend: "true",
                        legendText: "{label}",
                        dataPoints: self.totalClosedSales()
                    }]
                });
            }
        });
    } else {
        $("#navHeader").load("/header");
        self.isAdmin(false);
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            indexViewModel = new IndexViewModel();
            ko.applyBindings(indexViewModel);
        }
    });
});
