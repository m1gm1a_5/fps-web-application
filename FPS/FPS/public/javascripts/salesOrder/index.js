﻿'use strict';
var sessionObject = {};
var sessionJWT = {};
var salesOrderViewModel = ko.observable(null);

var authUrl = 'http://localhost:9001/api';
var customerUrl = 'http://localhost:9002/api';
var productionUrl = 'http://localhost:9003/api';

function SalesOrderViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);

    self.salesOrders = ko.observableArray([]);
    self.salesOrderStatuses = ko.observableArray([]);
    self.currentSalesOrder = ko.observable(null);

    self.fabrics = ko.observableArray([]);
    self.colors = ko.observableArray([]);
    self.colorGroups = ko.observableArray([]);
    self.stamps = ko.observableArray([]);
    self.processes = ko.observableArray([]);
    self.invoices = ko.observableArray([]);
    self.colorShow = ko.observable(false);
    self.stampShow = ko.observable(false);

    self.ToShortDateString = function (date) {
        return new Date(date).toLocaleDateString();
    }

    self.CheckUncompletedOrders = function () {
        var count = 0;
        var i = 0;
        for (i = 0; i < self.salesOrders().length; i++) {
            if (self.salesOrders()[i].status_id !== 3) {
                count++;
            }
        }

        return count;
    }

    self.CheckInvoice = function (spOrder) {
        if (spOrder.invoice_id === undefined || spOrder.invoice_id === null)
            return false;

        return true;
    }

    self.GetInvoiceXML = function (spOrder) {
        var data = {};
        var i = 0;
        for (i = 0; i < self.invoices().length; i++) {
            if (self.invoices()[i].id === spOrder.invoice_id) {
                data = self.invoices()[i];
                break;
            }
        }

        var d = new Date();
        var jsonData = {
            "filename": 'invoices/xml/invoice_' + d.toLocaleDateString().replace("/", "-").replace("/", "-") + '_' + d.toLocaleTimeString().replace(":", "-").replace(":", "-").replace(":", "-") + "_id_" + spOrder.id + '.xml',
            "data": data
        };

        $.ajax({
            type: 'POST',
            url: "/salesorder/writeXML/",
            contentType: "application/json",
            data: JSON.stringify(jsonData),
            success: function (ret) {
                var element = document.createElement('a');
                element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(new XMLSerializer().serializeToString(ret)));
                element.setAttribute('download', jsonData.filename);

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    alert('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.message);
                }
            }
        });
    }

    self.GetInvoiceJSON = function (spOrder) {
        var data = {};
        var i = 0;
        for (i = 0; i < self.invoices().length; i++) {
            if (self.invoices()[i].id === spOrder.invoice_id) {
                data = self.invoices()[i];
                break;
            }
        }

        var d = new Date();
        var jsonData = {
            "filename": 'invoices/json/invoice_' + d.toLocaleDateString().replace("/", "-").replace("/", "-") + '_' + d.toLocaleTimeString().replace(":", "-").replace(":", "-").replace(":", "-") + "_id_" + spOrder.id + '.json',
            "data": data
        };

        $.ajax({
            type: 'POST',
            url: "/salesorder/writeJSON/",
            contentType: "application/json",
            data: JSON.stringify(jsonData),
            success: function (ret) {
                var element = document.createElement('a');
                element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(ret)));
                element.setAttribute('download', jsonData.filename);

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    alert('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.message);
                }
            }
        });
    }

    self.ProcessCheck = function (process, event) {
        var splittedId = parseInt(event.target.id.split("_")[1]);
        var i = 0;
        if (event.target.checked) {
            for (i = 0; i < self.processes().length; i++) {
                if (self.processes()[i].id == splittedId) {
                    if (self.processes()[i].name == "Dyeing") {
                        self.colorShow(true);
                    } else if (self.processes()[i].name == "Stamping") {
                        self.stampShow(true);
                    }
                    break;
                }
            }
        } else {
            for (i = 0; i < self.processes().length; i++) {
                if (self.processes()[i].id == splittedId) {
                    if (self.processes()[i].name == "Dyeing") {
                        self.colorShow(false);
                    } else if (self.processes()[i].name == "Stamping") {
                        self.stampShow(false);
                    }
                    break;
                }
            }
        }
    }

    self.editSalesOrder = function (spOrder) {
        spOrder.saless = self.saless;
        spOrder.yarns = self.yarns;
        spOrder.currentSales = ko.observable(null);
        spOrder.currentYarn = ko.observable(null);
        var j = 0;
        for (j = 0; j < self.yarns().length; j++) {
            if (self.yarns()[j].id === spOrder.yarn_id) {
                spOrder.currentYarn(self.yarns()[j]);
                break;
            }
        }

        for (j = 0; j < self.saless().length; j++) {
            if (self.saless()[j].id === spOrder.status_id) {
                spOrder.currentSales(self.saless()[j]);
                break;
            }
        }

        self.currentSalesOrder(spOrder);
    };

    self.editConfirm = function (spOrder) {
        var jsonData = {
            "name": spOrder.code,
            "price": parseFloat(spOrder.price)
        };

        /*$.ajax({
            type: 'PUT',
            url: productionUrl + "/color/category/" + spOrder.id,
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function () {
                $('#salesOrderEditModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });*/
    };

    self.deleteSalesOrder = function (spOrder) {
        self.currentSalesOrder(spOrder);
    };

    self.deleteConfirm = function () {
        alert('Deleted ' + self.currentSalesOrder().code);
    };

    if (sessionObject.role !== undefined && sessionObject.role !== null) {
        if (sessionObject.role.slug === 'customer') {
            $("#navHeader").load("/header");
            self.isAdmin(false);
            $.ajax({
                type: 'GET',
                url: productionUrl + "/process",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.processes.push({
                            id: data[i].id,
                            name: data[i].name,
                            sequence: data[i].sequence,
                            is_optional: data[i].is_optional == 0 ? false : true,
                            price_per_kg: data[i].price_per_kg
                        });
                    }
                }
            });

            $.ajax({
                type: 'GET',
                url: productionUrl + "/fabric",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.fabrics.push({
                            id: data[i].id,
                            name: data[i].name,
                            composition: data[i].composition,
                            description: data[i].description,
                            yarns: data[i].FabricYarns
                        });
                    }
                }
            });

            $.ajax({
                type: 'GET',
                url: productionUrl + "/stamp",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.stamps.push({
                            id: data[i].id,
                            code: data[i].code,
                            stamp: data[i].stamp
                        });
                    }
                }
            });

            $.ajax({
                type: 'GET',
                url: productionUrl + "/color/category",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.colorGroups.push({
                            id: data[i].id,
                            name: data[i].name,
                            price: data[i].price
                        });
                    }
                    $.ajax({
                        type: 'GET',
                        url: productionUrl + "/color",
                        contentType: "application/json",
                        headers: { 'jwt': sessionJWT },
                        success: function (data) {
                            var i = 0;
                            for (i = 0; i < data.length; i++) {
                                var aux = {
                                    id: data[i].id,
                                    code: data[i].code,
                                    color_category_id: data[i].color_category_id
                                };

                                var j = 0;
                                for (j = 0; j < self.colorGroups().length; j++) {
                                    if (self.colorGroups()[j].id === aux.color_category_id) {
                                        aux.color_category_name = self.colorGroups()[j].name;
                                        aux.color_category_price = self.colorGroups()[j].price;
                                        break;
                                    }
                                }

                                self.colors.push(aux);
                            }
                        }
                    });
                }
            });

            $.ajax({
                type: 'GET',
                url: customerUrl + "/so/status",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.salesOrderStatuses.push({
                            id: data[i].id,
                            name: data[i].name,
                            code: data[i].code
                        });
                    }

                    $.ajax({
                        type: 'GET',
                        url: customerUrl + "/invoice",
                        contentType: "application/json",
                        headers: { 'jwt': sessionJWT },
                        success: function (data) {
                            var i = 0;
                            for (i = 0; i < data.length; i++) {
                                self.invoices.push({
                                    id: data[i].id,
                                    sales_order_id: data[i].sales_order_id,
                                    customer_id: data[i].customer_id,
                                    customer_name: data[i].customer_name,
                                    date: data[i].date,
                                    yarns_cost: data[i].yarns_cost,
                                    processes_cost: data[i].processes_cost,
                                    total_cost: data[i].total_cost,
                                    margin: data[i].margin,
                                    final_total: data[i].final_total,
                                    created_at: data[i].created_at,
                                    updated_at: data[i].updated_at
                                });
                            }

                            $.ajax({
                                type: 'GET',
                                url: customerUrl + "/so",
                                contentType: "application/json",
                                headers: { 'jwt': sessionJWT },
                                success: function (data) {
                                    var i = 0;
                                    for (i = 0; i < data.length; i++) {
                                        var aux = {
                                            id: data[i].id,
                                            customer_id: data[i].customer_id,
                                            fabric_id: data[i].fabric_id,
                                            fabric: data[i].fabric,
                                            quantity: data[i].quantity,
                                            color_code_id: data[i].color_code_id,
                                            color_code: data[i].color_code,
                                            print_code_id: data[i].print_code_id,
                                            print_code: data[i].print_code,
                                            delivery_date: data[i].delivery_date,
                                            delivery_address: data[i].delivery_address,
                                            delivery_zip: data[i].delivery_zip,
                                            delivery_city: data[i].delivery_city,
                                            delivery_country: data[i].delivery_country,
                                            status_id: data[i].status_id
                                        };

                                        var j = 0;
                                        for (j = 0; j < self.salesOrderStatuses().length; j++) {
                                            if (self.salesOrderStatuses()[j].id === aux.status_id) {
                                                aux.status_name = self.salesOrderStatuses()[j].name;
                                                break;
                                            }
                                        }

                                        var j = 0;
                                        for (j = 0; j < self.invoices().length; j++) {
                                            if (self.invoices()[j].sales_order_id === aux.id) {
                                                aux.invoice_id = self.invoices()[j].id;
                                                break;
                                            }
                                        }

                                        self.salesOrders.push(aux);
                                    }
                                }
                            });
                        }
                    });
                }
            });
        } else {
            $("#navHeader").load("/adminHeader");
            self.isAdmin(true);
        }
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            salesOrderViewModel = new SalesOrderViewModel();
            ko.applyBindings(salesOrderViewModel);
        }
    });

    $('#salesOrderForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        //validate date
        var d1 = Date.parse(dataObj["delivery_date"]);
        var d2 = new Date();
        if (d1 <= d2) {
            $('#alertForm').text('Invalid date: select an higher date!');
        } else {
            var jsonData = {
                "fabric_id": parseInt(dataObj["fabric"]),
                "delivery_address": dataObj["delivery_address"],
                "delivery_date": dataObj["delivery_date"],
                "delivery_zip": dataObj["delivery_zip"],
                "delivery_city": dataObj["delivery_city"],
                "delivery_country": dataObj["delivery_country"],
                "quantity": dataObj["quantity"]
            };

            var i = 0;
            for (j = 0; j < salesOrderViewModel.fabrics().length; j++) {
                if (salesOrderViewModel.fabrics()[j].id === parseInt(dataObj["fabric"])) {
                    jsonData.fabric = salesOrderViewModel.fabrics()[j].name;
                    break;
                }
            }

            for (i = 0; i < $('#processes').children().length; i++) {
                var splittedId = parseInt($($('#processes').children()[i]).children()[1].id.split("_")[1]);
                if (salesOrderViewModel.processes()[i].id == splittedId && $($('#processes').children()[i]).children()[1].checked) {
                    if (salesOrderViewModel.processes()[i].name == "Dyeing") {
                        var j = 0;
                        for (j = 0; j < salesOrderViewModel.colors().length; j++) {
                            if (salesOrderViewModel.colors()[j].id === parseInt(dataObj["color"])) {
                                jsonData.color_code_id = salesOrderViewModel.colors()[j].id;
                                jsonData.color_code = salesOrderViewModel.colors()[j].code;
                                break;
                            }
                        }
                    } else if (salesOrderViewModel.processes()[i].name == "Stamping") {
                        var j = 0;
                        for (j = 0; j < salesOrderViewModel.stamps().length; j++) {
                            if (salesOrderViewModel.stamps()[j].id === parseInt(dataObj["stamp"])) {
                                jsonData.print_code_id = salesOrderViewModel.stamps()[j].id;
                                jsonData.print_code = salesOrderViewModel.stamps()[j].code;
                                break;
                            }
                        }
                    }
                }
            }

            $.ajax({
                type: 'POST',
                url: customerUrl + "/so",
                data: JSON.stringify(jsonData),
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var j = 0;
                    var aux = {
                        id: data.id,
                        customer_id: data.customer_id,
                        delivery_address: data.delivery_address,
                        delivery_date: data.delivery_date,
                        delivery_country: data.delivery_country,
                        delivery_city: data.delivery_city,
                        delivery_zip: data.delivery_zip,
                        fabric: data.fabric,
                        fabric_id: data.fabric_id,
                        color_code: data.color_code,
                        color_code_id: data.color_code_id,
                        print_code: data.print_code,
                        print_code_id: data.print_code_id,
                        quantity: data.quantity,
                        status_id: data.status_id
                    };

                    for (j = 0; j < salesOrderViewModel.salesOrderStatuses().length; j++) {
                        if (salesOrderViewModel.salesOrderStatuses()[j].id === data.status_id) {
                            aux.status_name = salesOrderViewModel.salesOrderStatuses()[j].name;
                            break;
                        }
                    }

                    salesOrderViewModel.salesOrders.push(aux);
                    $('#salesOrderAddModal').modal('toggle');
                },
                error: function (xhr, status, error) {
                    if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                        $('#alertForm').text('An unexpected error has occured.');
                    } else {
                        var err = eval("(" + xhr.responseText + ")");
                        $('#alertForm').text(err.message);
                    }
                }
            });
        }

        event.preventDefault();
        return false;
    });
});
