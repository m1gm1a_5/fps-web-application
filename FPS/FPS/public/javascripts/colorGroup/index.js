﻿'use strict';
var sessionObject = {};
var sessionJWT = {};
var colorGroupViewModel = ko.observable(null);

var authUrl = 'http://localhost:9001/api';
var productionUrl = 'http://localhost:9003/api';

function ColorGroupViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);
    self.colorGroups = ko.observableArray([]);
    self.currentColorGroup = ko.observable(null);

    self.editColorGroup = function (cGroup) {
        self.currentColorGroup(cGroup);
    };

    self.editConfirm = function (cGroup) {
        var jsonData = {
            "name": cGroup.code,
            "price": parseFloat(cGroup.price)
        };

        $.ajax({
            type: 'PUT',
            url: productionUrl + "/color/category/" + cGroup.id,
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function () {
                $('#colorGroupEditModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });
    };

    self.deleteColorGroup = function (cGroup) {
        self.currentColorGroup(cGroup);
    };

    self.deleteConfirm = function () {
        alert('Deleted ' + self.currentColorGroup().code);
    };

    if (sessionObject.role !== undefined && sessionObject.role !== null) {
        if (sessionObject.role.slug === 'admin') {
            $("#navHeader").load("/adminHeader");
            self.isAdmin(true);
            $.ajax({
                type: 'GET',
                url: productionUrl + "/color/category",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.colorGroups.push({
                            id: data[i].id,
                            code: data[i].name,
                            price: data[i].price
                        });
                    }
                }
            });
        } else {
            $("#navHeader").load("/header");
            self.isAdmin(false);
        }
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            colorGroupViewModel = new ColorGroupViewModel();
            ko.applyBindings(colorGroupViewModel);
        }
    });

    $('#colorGroupForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        var jsonData = {
            "code": dataObj["code"],
            "price": parseFloat(dataObj["price"])
        };

        $.ajax({
            type: 'POST',
            url: productionUrl + "/color/category",
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                colorGroupViewModel.colorGroups.push({
                    id: data.id,
                    code: data.name,
                    price: data.price
                });
                $('#colorGroupAddModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });

        event.preventDefault();
        return false;
    });
});
