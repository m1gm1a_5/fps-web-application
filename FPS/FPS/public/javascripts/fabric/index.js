﻿'use strict';
var sessionObject = {};
var sessionJWT = {};
var fabricViewModel = ko.observable(null);

var authUrl = 'http://localhost:9001/api';
var productionUrl = 'http://localhost:9003/api';

function FabricViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);
    self.fabrics = ko.observableArray([]);
    self.currentFabric = ko.observable(null);

    self.yarns = ko.observableArray([]);
    self.blendYarns = ko.observableArray([]);

    self.showPlain = ko.observable(true);
    self.showBlend = ko.observable(false);

    self.editFabric = function (fabric) {
        var i = 0;
        for (i = 0; i < fabricViewModel.fabrics().length; i++) {
            if (fabricViewModel.fabrics()[i].id === fabric.id) {
                fabric.yarns = fabricViewModel.fabrics()[i].yarns;
                break;
            }
        }

        if (fabric.yarns.length === 1) {
            fabric.showPlain = ko.observable(true);
            fabric.showBlend = ko.observable(false);
        } else {
            fabric.showPlain = ko.observable(false);
            fabric.showBlend = ko.observable(true);
        }
        self.currentFabric(fabric);
    };

    self.ValidationChange = function (fabric, event) {
        if (event.target.value === "blend") {
            fabricViewModel.showPlain(false);
            fabricViewModel.showBlend(true);
        } else {
            fabricViewModel.showPlain(true);
            fabricViewModel.showBlend(false);
        }
    };

    self.editConfirm = function (fabric) {
        var jsonData = {
            "name": fabric.code,
            "price": parseFloat(fabric.price)
        };

        $.ajax({
            type: 'PUT',
            url: productionUrl + "/color/category/" + fabric.id,
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function () {
                $('#fabricEditModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });
    };

    self.deleteFabric = function (fabric) {
        self.currentFabric(fabric);
    };

    self.deleteConfirm = function () {
        alert('Deleted ' + self.currentFabric().code);
    };

    if (sessionObject.role !== undefined && sessionObject.role !== null) {
        if (sessionObject.role.slug === 'admin') {
            $("#navHeader").load("/adminHeader");
            self.isAdmin(true);
        } else {
            $("#navHeader").load("/header");
            self.isAdmin(false);
        }

        $.ajax({
            type: 'GET',
            url: productionUrl + "/fabric",
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                var i = 0;
                for (i = 0; i < data.length; i++) {
                    self.fabrics.push({
                        id: data[i].id,
                        name: data[i].name,
                        composition: data[i].composition,
                        description: data[i].description,
                        yarns: data[i].FabricYarns
                    });
                }
            }
        });

        $.ajax({
            type: 'GET',
            url: productionUrl + "/yarn",
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                var i = 0;
                for (i = 0; i < data.length; i++) {
                    self.yarns.push({
                        id: data[i].id,
                        name: data[i].name,
                        code: data[i].code,
                        ne_title: data[i].ne_title,
                        n_cables: data[i].n_cables,
                        n_filaments: data[i].n_filaments,
                        tpm: data[i].tpm,
                        price: data[i].price
                    });
                }

                for (i = 0; i < 4; i++) {
                    self.blendYarns.push({
                        bl_id: i,
                        bl_quantity: 25,
                        bl_yarns: self.yarns()
                    });
                }
            }
        });
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            fabricViewModel = new FabricViewModel();
            ko.applyBindings(fabricViewModel);
        }
    });

    $('#typeGroup').on("change", function (event) {
        if (event.target.value === "blend") {
            fabricViewModel.showPlain(false);
            fabricViewModel.showBlend(true);
        } else {
            fabricViewModel.showPlain(true);
            fabricViewModel.showBlend(false);
        }
    });

    $('#fabricForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        var jsonData = {
            "name": dataObj["name"],
            "description": dataObj["description"]
        };

        if (dataObj["typeGroup"] === "blend") {
            if (parseInt(dataObj["bl_quantity_name_0"]) + parseInt(dataObj["bl_quantity_name_1"]) + parseInt(dataObj["bl_quantity_name_2"]) + parseInt(dataObj["bl_quantity_name_3"]) !== 100) {
                $('#alertForm').text('The sum of quantities should be 100!');
                event.preventDefault();
                return false;
            }

            var auxArray = [];
            var yarnArray = [];

            if (parseInt(dataObj["bl_quantity_name_0"]) !== 0) {
                auxArray.push({
                    yarn: dataObj["bl_yarn_name_0"],
                    quantity: parseInt(dataObj["bl_quantity_name_0"])
                });
            }

            if (parseInt(dataObj["bl_quantity_name_1"]) !== 0) {
                auxArray.push({
                    yarn: dataObj["bl_yarn_name_1"],
                    quantity: parseInt(dataObj["bl_quantity_name_1"])
                });
            }

            if (parseInt(dataObj["bl_quantity_name_2"]) !== 0) {
                auxArray.push({
                    yarn: dataObj["bl_yarn_name_2"],
                    quantity: parseInt(dataObj["bl_quantity_name_2"])
                });
            }

            if (parseInt(dataObj["bl_quantity_name_3"]) !== 0) {
                auxArray.push({
                    yarn: dataObj["bl_yarn_name_3"],
                    quantity: parseInt(dataObj["bl_quantity_name_3"])
                });
            }
            
            var i = 0;
            for (i = 0; i < auxArray.length; i++) {
                if (yarnArray === []) {
                    yarnArray.push(auxArray[i]);
                } else {
                    var check = false;
                    var j = 0;
                    for (j = 0; j < yarnArray.length; j++) {
                        if (auxArray[i].yarn === yarnArray[j].yarn) {
                            check = true;
                            yarnArray[j].quantity += auxArray[i].quantity;
                            break;
                        }
                    }

                    if (!check) {
                        yarnArray.push(auxArray[i]);
                    }
                }
            }

            jsonData.yarns = [];

            for (i = 0; i < yarnArray.length; i++) {
                jsonData.yarns.push({
                    yarn_id: parseInt(yarnArray[i].yarn),
                    yarn_percentage: yarnArray[i].quantity
                });
            }
        } else {
            jsonData.yarns = [{
                yarn_id: parseInt(dataObj["yarn"]),
                yarn_percentage: 100
            }];
        }

        $.ajax({
            type: 'POST',
            url: productionUrl + "/fabric",
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                fabricViewModel.fabrics.push({
                    id: data.id,
                    name: data.name,
                    description: data.description,
                    composition: data.composition,
                    yarns: data.yarns
                });
                $('#fabricAddModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });

        event.preventDefault();
        return false;
    });
});
