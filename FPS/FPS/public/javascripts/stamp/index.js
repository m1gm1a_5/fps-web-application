﻿'use strict';
var sessionObject = {};
var sessionJWT = {};
var stampViewModel = ko.observable(null);

var authUrl = 'http://localhost:9001/api';
var productionUrl = 'http://localhost:9003/api';

function StampViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);
    self.stamps = ko.observableArray([]);
    self.currentStamp = ko.observable(null);

    self.editStamp = function (stamp) {
        self.currentStamp(stamp);
    };

    self.editConfirm = function (stamp) {
        var jsonData = {
            "code": stamp.code,
            "stamp": stamp.stamp
        };

        $.ajax({
            type: 'PUT',
            url: productionUrl + "/stamp/" + stamp.id,
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function () {
                $('#stampEditModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });
    };

    self.deleteStamp = function (stamp) {
        self.currentStamp(stamp);
    };

    self.deleteConfirm = function () {
        alert('Deleted ' + self.currentStamp().code);
    };

    if (sessionObject.role !== undefined && sessionObject.role !== null) {
        if (sessionObject.role.slug === 'admin') {
            $("#navHeader").load("/adminHeader");
            self.isAdmin(true);
        } else {
            $("#navHeader").load("/header");
            self.isAdmin(false);
        }

        $.ajax({
            type: 'GET',
            url: productionUrl + "/stamp",
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                var i = 0;
                for (i = 0; i < data.length; i++) {
                    self.stamps.push({
                        id: data[i].id,
                        code: data[i].code,
                        stamp: data[i].stamp
                    });
                }
            }
        });
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            stampViewModel = new StampViewModel();
            ko.applyBindings(stampViewModel);
        }
    });

    $('#importCSV').on("change", function () {
        if (this.files.length !== 0) {
            var fd = new FormData();
            fd.append('file', this.files[0]);

            $.ajax({
                type: 'POST',
                url: productionUrl + '/stamp/import',
                dataType: 'text',
                contentType: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: fd,
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    location.reload();
                },
                error: function (xhr, status, error) {
                    if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                        alert('An unexpected error has occured.');
                    } else {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.message);
                    }
                }
            });
        }
    });

    $('#stampForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        var jsonData = {
            "code": dataObj["code"],
            "stamp": dataObj["stamp"]
        };

        $.ajax({
            type: 'POST',
            url: productionUrl + "/stamp",
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                stampViewModel.stamps.push({
                    id: data.id,
                    code: data.code,
                    stamp: data.stamp
                });
                $('#stampAddModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });

        event.preventDefault();
        return false;
    });
});
