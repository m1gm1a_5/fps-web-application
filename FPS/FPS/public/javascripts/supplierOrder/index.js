﻿'use strict';
var sessionObject = {};
var sessionJWT = {};
var supplierOrderViewModel = ko.observable(null);

var authUrl = 'http://localhost:9001/api';
var supplierUrl = 'http://localhost:9000/api';
var productionUrl = 'http://localhost:9003/api';

function SupplierOrderViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);
    self.supplierOrders = ko.observableArray([]);
    self.supplierOrderStatuses = ko.observableArray([]);
    self.suppliers = ko.observableArray([]);
    self.yarns = ko.observableArray([]);
    self.currentSupplierOrder = ko.observable(null);

    self.editSupplierOrder = function (spOrder) {
        spOrder.suppliers = self.suppliers;
        spOrder.yarns = self.yarns;
        spOrder.currentSupplier = ko.observable(null);
        spOrder.currentYarn = ko.observable(null);
        var j = 0;
        for (j = 0; j < self.yarns().length; j++) {
            if (self.yarns()[j].id === spOrder.yarn_id) {
                spOrder.currentYarn(self.yarns()[j]);
                break;
            }
        }

        for (j = 0; j < self.suppliers().length; j++) {
            if (self.suppliers()[j].id === spOrder.status_id) {
                spOrder.currentSupplier(self.suppliers()[j]);
                break;
            }
        }

        self.currentSupplierOrder(spOrder);
    };

    self.editConfirm = function (spOrder) {
        var jsonData = {
            "name": spOrder.code,
            "price": parseFloat(spOrder.price)
        };

        /*$.ajax({
            type: 'PUT',
            url: productionUrl + "/color/category/" + spOrder.id,
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function () {
                $('#supplierOrderEditModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });*/
    };

    self.deleteSupplierOrder = function (spOrder) {
        self.currentSupplierOrder(spOrder);
    };

    self.deleteConfirm = function () {
        alert('Deleted ' + self.currentSupplierOrder().code);
    };

    if (sessionObject.role !== undefined && sessionObject.role !== null) {
        if (sessionObject.role.slug === 'admin') {
            $("#navHeader").load("/adminHeader");
            self.isAdmin(true);
            $.ajax({
                type: 'GET',
                url: productionUrl + "/yarn",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.yarns.push({
                            id: data[i].id,
                            name: data[i].name,
                            code: data[i].code,
                            ne_title: data[i].ne_title,
                            n_cables: data[i].n_cables,
                            n_filaments: data[i].n_filaments,
                            tpm: data[i].tpm,
                            price: data[i].price
                        });
                    }

                    $.ajax({
                        type: 'GET',
                        url: supplierUrl + "/spro/status",
                        contentType: "application/json",
                        headers: { 'jwt': sessionJWT },
                        success: function (data) {
                            var i = 0;
                            for (i = 0; i < data.length; i++) {
                                self.supplierOrderStatuses.push({
                                    id: data[i].id,
                                    name: data[i].name,
                                    code: data[i].code
                                });
                            }

                            $.ajax({
                                type: 'GET',
                                url: supplierUrl + "/spro",
                                contentType: "application/json",
                                headers: { 'jwt': sessionJWT },
                                success: function (data) {
                                    var i = 0;
                                    for (i = 0; i < data.length; i++) {
                                        var j = 0;
                                        var aux = {
                                            id: data[i].id,
                                            supplier_id: data[i].supplier_id,
                                            supplier_name: data[i].supplier_name,
                                            date_of_delivery: data[i].date_of_delivery,
                                            yarn_id: data[i].yarn_id,
                                            yarn_quantity: data[i].yarn_quantity,
                                            request_user_id: data[i].request_user_id,
                                            status_id: data[i].status_id,
                                            status_name: ""
                                        };

                                        for (j = 0; j < self.yarns().length; j++) {
                                            if (self.yarns()[j].id === data[i].yarn_id) {
                                                aux.yarn_name = self.yarns()[j].name;
                                                break;
                                            }
                                        }

                                        for (j = 0; j < self.supplierOrderStatuses().length; j++) {
                                            if (self.supplierOrderStatuses()[j].id === data[i].status_id) {
                                                aux.status_name = self.supplierOrderStatuses()[j].name;
                                                break;
                                            }
                                        }

                                        self.supplierOrders.push(aux);
                                    }
                                }
                            });
                        }
                    });

                    
                }
            });

            $.ajax({
                type: 'GET',
                url: supplierUrl + "/supplier",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.suppliers.push({
                            id: data[i].id,
                            name: data[i].name,
                            phone: data[i].phone,
                            address: data[i].address,
                            zip: data[i].zip,
                            city: data[i].city,
                            country: data[i].country,
                            contact_person: data[i].contact_person
                        });
                    }
                }
            });

            

        } else {
            $("#navHeader").load("/header");
            self.isAdmin(false);
        }
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            supplierOrderViewModel = new SupplierOrderViewModel();
            ko.applyBindings(supplierOrderViewModel);
        }
    });

    $('#supplierOrderForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        //validate date
        var d1 = Date.parse(dataObj["date_of_delivery"]);
        var d2 = new Date();
        if (d1 <= d2) {
            $('#alertForm').text('Invalid date: select an higher date!');
        } else {
            var jsonData = {
                "supplier_id": parseInt(dataObj["supplier_name"]),
                "yarn_id": parseInt(dataObj["yarn_name"]),
                "yarn_quantity": parseInt(dataObj["yarn_quantity"]),
                "date_of_delivery": dataObj["date_of_delivery"],
                "status_id": 1
            };

            var j = 0;
            for (j = 0; j < supplierOrderViewModel.yarns().length; j++) {
                if (supplierOrderViewModel.yarns()[j].id === jsonData.yarn_id) {
                    jsonData.yarn_name = supplierOrderViewModel.yarns()[j].name;
                    break;
                }
            }

            for (j = 0; j < supplierOrderViewModel.suppliers().length; j++) {
                if (supplierOrderViewModel.suppliers()[j].id === jsonData.status_id) {
                    jsonData.supplier_name = supplierOrderViewModel.suppliers()[j].name;
                    break;
                }
            }

            $.ajax({
                type: 'POST',
                url: supplierUrl + "/spro",
                data: JSON.stringify(jsonData),
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var j = 0;
                    var aux = {
                        id: data.id,
                        supplier_id: data.supplier_id,
                        supplier_name: data.supplier_name,
                        date_of_delivery: data.date_of_delivery,
                        yarn_id: data.yarn_id,
                        yarn_quantity: data.yarn_quantity,
                        request_user_id: data.request_user_id,
                        status_id: data.status_id
                    };

                    for (j = 0; j < supplierOrderViewModel.yarns().length; j++) {
                        if (supplierOrderViewModel.yarns()[j].id === data.yarn_id) {
                            aux.yarn_name = supplierOrderViewModel.yarns()[j].name;
                            break;
                        }
                    }

                    for (j = 0; j < supplierOrderViewModel.supplierOrderStatuses().length; j++) {
                        if (supplierOrderViewModel.supplierOrderStatuses()[j].id === data.status_id) {
                            aux.status_name = supplierOrderViewModel.supplierOrderStatuses()[j].name;
                            break;
                        }
                    }

                    supplierOrderViewModel.supplierOrders.push(aux);
                    $('#supplierOrderAddModal').modal('toggle');
                },
                error: function (xhr, status, error) {
                    if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                        $('#alertForm').text('An unexpected error has occured.');
                    } else {
                        var err = eval("(" + xhr.responseText + ")");
                        $('#alertForm').text(err.message);
                    }
                }
            });
        }

        event.preventDefault();
        return false;
    });
});
