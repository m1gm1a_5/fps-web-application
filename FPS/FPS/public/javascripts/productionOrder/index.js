﻿'use strict';
var sessionObject = {};
var sessionJWT = {};
var productionOrderViewModel = ko.observable(null);

var authUrl = 'http://localhost:9001/api';
var productionUrl = 'http://localhost:9003/api';

function ProductionOrderViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);
    self.productionOrders = ko.observableArray([]);
    self.currentProductionOrder = ko.observable(null);

    self.productionOrderStatuses = ko.observableArray([]);
    self.stamps = ko.observableArray([]);
    self.colors = ko.observableArray([]);
    self.fabrics = ko.observableArray([]);
    self.processes = ko.observableArray([]);

    self.MoveProductionOrder = function (cGroup) {
        if (cGroup.status_id === 1) {
            var i = 0;
            var stockCheck = true;
            for (i = 0; i < cGroup.fabric_yarns.length; i++) {
                if (stockCheck) {
                    var auxName = cGroup.fabric_yarns[i].Yarn.name;
                    $.ajax({
                        type: 'GET',
                        url: productionUrl + "/stock/" + cGroup.fabric_yarns[i].yarn_id,
                        contentType: "application/json",
                        headers: { 'jwt': sessionJWT },
                        async: false,
                        success: function (data) {
                        },
                        error: function (xhr, status, error) {
                            if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                                alert('An unexpected error has occured.');
                            } else {
                                var err = eval("(" + xhr.responseText + ")");
                                alert(auxName + ": " + err.message);
                                stockCheck = false;
                                return;
                            }
                        }
                    });
                }
            }

            if (stockCheck) {
                $.ajax({
                    type: 'PUT',
                    url: productionUrl + "/production/started/" + cGroup.id,
                    contentType: "application/json",
                    headers: { 'jwt': sessionJWT },
                    success: function (data) {
                        location.reload();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                            alert('An unexpected error has occured.');
                        } else {
                            var err = eval("(" + xhr.responseText + ")");
                            alert(err.message);
                        }
                    }
                });
            }
        } else if (cGroup.status_id === 2) {
            if (cGroup.process_id === 7) {
                var jsonData = {
                    "process_id": cGroup.process_id
                };
                $.ajax({
                    type: 'PUT',
                    url: productionUrl + "/production/finished/" + cGroup.id,
                    contentType: "application/json",
                    data: JSON.stringify(jsonData),
                    headers: { 'jwt': sessionJWT },
                    success: function (data) {
                        location.reload();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                            alert('An unexpected error has occured.');
                        } else {
                            var err = eval("(" + xhr.responseText + ")");
                            alert(err.message);
                        }
                    }
                });
            } else {
                var jsonData = {
                    "process_id": cGroup.process_id + 1,
                    "status_id": cGroup.status_id
                };

                if (jsonData.process_id === 3 && cGroup.color_id === null)
                    jsonData.process_id++;

                if (jsonData.process_id === 4 && cGroup.stamp_id === null)
                    jsonData.process_id++;

                $.ajax({
                    type: 'PUT',
                    url: productionUrl + "/production/" + cGroup.id,
                    contentType: "application/json",
                    data: JSON.stringify(jsonData),
                    headers: { 'jwt': sessionJWT },
                    success: function (data) {
                        location.reload();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                            alert('An unexpected error has occured.');
                        } else {
                            var err = eval("(" + xhr.responseText + ")");
                            alert(err.message);
                        }
                    }
                });
            }
        }
    };

    self.deleteProductionOrder = function (cGroup) {
        self.currentProductionOrder(cGroup);
    };

    self.deleteConfirm = function () {
        alert('Deleted ' + self.currentProductionOrder().code);
    };

    if (sessionObject.role !== undefined && sessionObject.role !== null) {
        if (sessionObject.role.slug === 'admin') {
            $("#navHeader").load("/adminHeader");
            self.isAdmin(true);

            $.ajax({
                type: 'GET',
                url: productionUrl + "/fabric",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.fabrics.push({
                            id: data[i].id,
                            name: data[i].name,
                            composition: data[i].composition,
                            description: data[i].description,
                            yarns: data[i].FabricYarns
                        });
                    }

                    $.ajax({
                        type: 'GET',
                        url: productionUrl + "/process",
                        contentType: "application/json",
                        headers: { 'jwt': sessionJWT },
                        success: function (data) {
                            var i = 0;
                            for (i = 0; i < data.length; i++) {
                                self.processes.push({
                                    id: data[i].id,
                                    name: data[i].name,
                                    sequence: data[i].sequence,
                                    is_optional: data[i].is_optional,
                                    price_per_kg: data[i].is_price_per_kg
                                });
                            }

                            $.ajax({
                                type: 'GET',
                                url: productionUrl + "/stamp",
                                contentType: "application/json",
                                headers: { 'jwt': sessionJWT },
                                success: function (data) {
                                    var i = 0;
                                    for (i = 0; i < data.length; i++) {
                                        self.stamps.push({
                                            id: data[i].id,
                                            code: data[i].code,
                                            stamp: data[i].stamp
                                        });
                                    }

                                    $.ajax({
                                        type: 'GET',
                                        url: productionUrl + "/color",
                                        contentType: "application/json",
                                        headers: { 'jwt': sessionJWT },
                                        success: function (data) {
                                            var i = 0;
                                            for (i = 0; i < data.length; i++) {
                                                self.colors.push({
                                                    id: data[i].id,
                                                    code: data[i].code,
                                                    color_category_id: data[i].color_category_id
                                                });
                                            }

                                            $.ajax({
                                                type: 'GET',
                                                url: productionUrl + "/production/status",
                                                contentType: "application/json",
                                                headers: { 'jwt': sessionJWT },
                                                success: function (data) {
                                                    var i = 0;
                                                    for (i = 0; i < data.length; i++) {
                                                        self.productionOrderStatuses.push({
                                                            id: data[i].id,
                                                            name: data[i].name,
                                                            code: data[i].code
                                                        });
                                                    }

                                                    $.ajax({
                                                        type: 'GET',
                                                        url: productionUrl + "/production",
                                                        contentType: "application/json",
                                                        headers: { 'jwt': sessionJWT },
                                                        success: function (data) {
                                                            var i = 0;
                                                            for (i = 0; i < data.length; i++) {
                                                                var aux = {
                                                                    id: data[i].id,
                                                                    sales_order_id: data[i].sales_order_id,
                                                                    process_id: data[i].process_id,
                                                                    fabric_id: data[i].fabric_id,
                                                                    fabric_name: null,
                                                                    quantity: data[i].quantity,
                                                                    color_id: data[i].color_id,
                                                                    color_code: null,
                                                                    stamp_id: data[i].stamp_id,
                                                                    print_code: null,
                                                                    status_id: data[i].status_id,
                                                                    status_name: null
                                                                };

                                                                var j = 0;
                                                                for (j = 0; j < self.fabrics().length; j++) {
                                                                    if (aux.fabric_id === self.fabrics()[j].id) {
                                                                        aux.fabric_name = self.fabrics()[j].name;
                                                                        aux.fabric_yarns = self.fabrics()[j].yarns;
                                                                        break;
                                                                    }
                                                                }

                                                                for (j = 0; j < self.productionOrderStatuses().length; j++) {
                                                                    if (aux.status_id === self.productionOrderStatuses()[j].id) {
                                                                        aux.status_name = self.productionOrderStatuses()[j].name;
                                                                        aux.status_code = self.productionOrderStatuses()[j].code;
                                                                        break;
                                                                    }
                                                                }

                                                                if (aux.process_id !== null) {
                                                                    var j = 0;
                                                                    for (j = 0; j < self.processes().length; j++) {
                                                                        if (aux.process_id === self.processes()[j].id) {
                                                                            aux.process_name = self.processes()[j].name;
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                                if (aux.color_id !== null) {
                                                                    var j = 0;
                                                                    for (j = 0; j < self.colors().length; j++) {
                                                                        if (aux.color_id === self.colors()[j].id) {
                                                                            aux.color_code = self.colors()[j].code;
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                                if (aux.stamp_id !== null) {
                                                                    var j = 0;
                                                                    for (j = 0; j < self.stamps().length; j++) {
                                                                        if (aux.stamp_id === self.stamps()[j].id) {
                                                                            aux.print_name = self.stamps()[j].name;
                                                                            aux.print_code = self.stamps()[j].code;
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                                self.productionOrders.push(aux);
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            $("#navHeader").load("/header");
            self.isAdmin(false);
        }
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            productionOrderViewModel = new ProductionOrderViewModel();
            ko.applyBindings(productionOrderViewModel);
        }
    });
});
