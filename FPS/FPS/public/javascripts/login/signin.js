﻿'use strict';

var authUrl = 'http://localhost:9001/api';
var customerUrl = 'http://localhost:9002/api';

$(document).ready(function () {
    $('#signInForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        if (dataObj['password'] !== dataObj['confirmpassword']) {
            $('#alertForm').text("The passwords don't match!");
            return false;
        }

        var userJsonData = {
            "username": dataObj["username"],
            "email": dataObj["email"],
            "password": dataObj["password"]
        };

        $.ajax({
            type: 'POST',
            url: authUrl + "/register",
            data: JSON.stringify(userJsonData),
            contentType: "application/json",
            success: function (data) {
                var base64Url = data.jwt.split('.')[1];
                var sessionObject = JSON.parse(window.atob(base64Url));

                var customerJsonData = {
                    "name": dataObj["name"],
                    "phone": dataObj["phone"],
                    "address": dataObj["address"],
                    "zip": dataObj["zip"],
                    "city": dataObj["city"],
                    "country": dataObj["country"],
                    "contact_person": dataObj["contact_person"]
                };

                $.ajax({
                    type: 'POST',
                    url: customerUrl + '/customer',
                    data: JSON.stringify(customerJsonData),
                    contentType: "application/json",
                    headers:{'jwt':data.jwt},
                    success: function () {
                        $.ajax({
                            type: 'POST',
                            url: '/auth',
                            data: JSON.stringify(data),
                            contentType: "application/json",
                            success: function () {
                                var getUrl = window.location;
                                var baseUrl = getUrl.protocol + "//" + getUrl.host + "/"; // + getUrl.pathname.split('/')[1]
                                window.open(baseUrl, "_self");
                            }
                        });
                    },
                    error: function (xhr, status, error) {
                        if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                            $('#alertForm').text('An unexpected error has occured.');
                        } else {
                            var err = eval("(" + xhr.responseText + ")");
                            $('#alertForm').text(err.message);
                        }
                    }
                });
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });

        event.preventDefault();
        return false;
    });
});
