﻿'use strict';

var authUrl = 'http://localhost:9001/api';

$(document).ready(function () {
    $('#logInForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        var jsonData = {
            "email": dataObj["email"],
            "password": dataObj["password"]
        };

        $.ajax({
            type: 'POST',
            url: authUrl + "/authenticate",
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            success: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '/auth',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function () {
                        location.reload();
                    }
                });
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });

        event.preventDefault();
        return false;
    });
});
