﻿'use strict';
var sessionObject = {};
var sessionJWT = {};
var colorViewModel = ko.observable(null);

var authUrl = 'http://localhost:9001/api';
var productionUrl = 'http://localhost:9003/api';

function ColorViewModel() {
    var self = this;
    self.isAdmin = ko.observable(false);
    self.colors = ko.observableArray([]);
    self.colorGroups = ko.observableArray([]);
    self.currentColor = ko.observable(null);

    self.FullName = function (fabric) {
        return fabric.code;
    };

    self.editColor = function (cGroup) {
        var i = 0;
        for (i = 0; i < colorViewModel.colorGroups().length; i++) {
            if (colorViewModel.colorGroups()[i].id === cGroup.color_category_id) {
                cGroup.currentColorGroup = ko.observable({
                    id: colorViewModel.colorGroups()[i].id,
                    code: colorViewModel.colorGroups()[i].name,
                    price: colorViewModel.colorGroups()[i].price
                });
                break;
            }
        }
        self.currentColor(cGroup);
    };

    self.editConfirm = function (cGroup) {
        var jsonData = {
            "name": cGroup.code,
            "price": parseFloat(cGroup.price)
        };

        $.ajax({
            type: 'PUT',
            url: productionUrl + "/color/category/" + cGroup.id,
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function () {
                $('#colorEditModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });
    };

    self.deleteColor = function (cGroup) {
        self.currentColor(cGroup);
    };

    self.deleteConfirm = function () {
        alert('Deleted ' + self.currentColor().code);
    };

    if (sessionObject.role !== undefined && sessionObject.role !== null) {
        if (sessionObject.role.slug === 'admin') {
            $("#navHeader").load("/adminHeader");
            self.isAdmin(true);
            $.ajax({
                type: 'GET',
                url: productionUrl + "/color/category",
                contentType: "application/json",
                headers: { 'jwt': sessionJWT },
                success: function (data) {
                    var i = 0;
                    for (i = 0; i < data.length; i++) {
                        self.colorGroups.push({
                            id: data[i].id,
                            code: data[i].name,
                            price: data[i].price
                        });
                    }

                    $.ajax({
                        type: 'GET',
                        url: productionUrl + "/color",
                        contentType: "application/json",
                        headers: { 'jwt': sessionJWT },
                        success: function (data) {
                            var i = 0;
                            for (i = 0; i < data.length; i++) {
                                var aux = {
                                    id: data[i].id,
                                    code: data[i].code,
                                    color_category_id: data[i].color_category_id
                                };

                                var j = 0;
                                for (j = 0; j < self.colorGroups().length; j++) {
                                    if (self.colorGroups()[j].id === aux.color_category_id) {
                                        aux.color_category_name = self.colorGroups()[j].code;
                                        aux.color_category_price = self.colorGroups()[j].price;
                                        break;
                                    }
                                }

                                self.colors.push(aux);
                            }
                        }
                    });
                }
            });
        } else {
            $("#navHeader").load("/header");
            self.isAdmin(false);
        }
    }
}

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/auth',
        contentType: "application/json",
        success: function (data) {
            sessionJWT = data;
            var base64Url = data.split('.')[1];
            sessionObject = JSON.parse(window.atob(base64Url));
            colorViewModel = new ColorViewModel();
            ko.applyBindings(colorViewModel);
        }
    });

    $('#colorForm').submit(function (event) {
        $('#alertForm').text('');
        var dataArray = $(this).serializeArray(),
            dataObj = {};

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        var jsonData = {
            "code": dataObj["code"],
            "color_category_id": parseInt(dataObj["colorcategory"])
        };

        $.ajax({
            type: 'POST',
            url: productionUrl + "/color",
            data: JSON.stringify(jsonData),
            contentType: "application/json",
            headers: { 'jwt': sessionJWT },
            success: function (data) {
                var aux = {
                    id: data.id,
                    code: data.code,
                    color_category_id: data.color_category_id
                }
                
                var j = 0;
                for (j = 0; j < colorViewModel.colorGroups().length; j++) {
                    if (colorViewModel.colorGroups()[j].id === aux.color_category_id) {
                        aux.color_category_name = colorViewModel.colorGroups()[j].code;
                        aux.color_category_price = colorViewModel.colorGroups()[j].price;
                        break;
                    }
                }

                colorViewModel.colors.push(aux);
                $('#colorAddModal').modal('toggle');
            },
            error: function (xhr, status, error) {
                if (xhr.responseText === undefined || xhr.responseText === null || xhr.responseText === '') {
                    $('#alertForm').text('An unexpected error has occured.');
                } else {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#alertForm').text(err.message);
                }
            }
        });

        event.preventDefault();
        return false;
    });
});
