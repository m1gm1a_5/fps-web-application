﻿"use strict";

$(document).ready(function () {
    $("#homeBtn").on("click", function () {
        var getUrl = window.location;
        var colorGroupUrl = getUrl.protocol + "//" + getUrl.host + "/";
        window.open(colorGroupUrl, "_self");
    });

    $("#productionOrderBtn").on("click", function () {
        var getUrl = window.location;
        var supplierOrderUrl = getUrl.protocol + "//" + getUrl.host + "/productionorder";
        window.open(supplierOrderUrl, "_self");
    });

    $("#supplierOrderBtn").on("click", function () {
        var getUrl = window.location;
        var supplierOrderUrl = getUrl.protocol + "//" + getUrl.host + "/supplierorder";
        window.open(supplierOrderUrl, "_self");
    });

    $("#fabricBtn").on("click", function () {
            var getUrl = window.location;
            var salesOrderUrl = getUrl.protocol + "//" + getUrl.host + "/fabric";
            window.open(salesOrderUrl, "_self");
    });

    $("#colorGroupBtn").on("click", function () {
        var getUrl = window.location;
        var colorGroupUrl = getUrl.protocol + "//" + getUrl.host + "/colorgroup";
        window.open(colorGroupUrl, "_self");
    });

    $("#colorBtn").on("click", function () {
        var getUrl = window.location;
        var colorGroupUrl = getUrl.protocol + "//" + getUrl.host + "/color";
        window.open(colorGroupUrl, "_self");
    });

    $("#stampBtn").on("click", function () {
        var getUrl = window.location;
        var colorGroupUrl = getUrl.protocol + "//" + getUrl.host + "/stamp";
        window.open(colorGroupUrl, "_self");
    });

    $("#logoutBtn").on("click", function () {
        $.ajax({
            type: 'POST',
            url: '/logout',
            contentType: "application/json",
            success: function () {
                var getUrl = window.location;
                var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
                window.open(baseUrl, "_self");
            }
        });
    });
});