﻿'use strict';
var express = require('express');
var path = require('path');
var reqPath = path.join(__dirname, 'views/');
var config = require('./server/config');
var app = express();
var router = express.Router();
var http = require('http');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var fs = require('fs');
var js2xmlparser = require('js2xmlparser');

var authentication = require('./routes/authentication');
var color = require('./routes/color');
var colorGroup = require('./routes/colorGroup');
var fabric = require('./routes/fabric');
var salesOrder = require('./routes/salesOrder');
var productionOrder = require('./routes/productionOrder');
var supplierOrder = require('./routes/supplierOrder');
var stamp = require('./routes/stamp');

var authenticate = function(req) {
    if (req.session.jwt === undefined || req.session.jwt === null || req.session.jwt === '')
        return false;

    return true;
};

router.use(cookieParser());
router.use(session({
    resave: false,
    saveUninitialized: true,
    secret: config.secret,
    cookie: { secure: false }
}));

router.use(function (req, res, next) {
    console.log("/" + req.method);
    next();
});

router.use(bodyParser.json());

router.get("/", function (req, res) {
    if (authenticate(req)) {
        res.sendFile(reqPath + "index.html");
        return;
    } else {
        return res.redirect('/login');
    }
});

router.post("/salesorder/writeXML", function (req, res) {
    var xmlObj = js2xmlparser.parse("person", req.body.data);
    console.log(xmlObj);
    fs.writeFile('./' + req.body.filename, xmlObj, 'utf-8', (err) => {
        if (err) throw err;
        res.sendFile(path.join(__dirname, "/" + req.body.filename));
    });
});

router.post("/salesorder/writeJSON", function (req, res) {
    console.log(req.body.data);
    fs.writeFile('./' + req.body.filename, JSON.stringify(req.body.data), 'utf-8', (err) => {
        if (err) throw err;
        res.sendFile(path.join(__dirname, "/" + req.body.filename));
    });
});

//Partials
router.get('/header', function (req, res) {
    res.sendFile(reqPath + "_partial/_header.html");
});

router.get('/adminHeader', function (req, res) {
    res.sendFile(reqPath + "_partial/_adminHeader.html");
});

// Authentication
router.post("/auth", authentication.setAuth);
router.get("/auth", authentication.getAuth);
router.post("/logout", authentication.logout);
router.get("/login", authentication.login);
router.get("/signin", authentication.signIn);

//Color
router.get("/color", color.color);

//Color Group
router.get("/colorgroup", colorGroup.colorGroup);

//Fabric
router.get("/fabric", fabric.fabric);

//Sales Order
router.get("/salesorder", salesOrder.salesOrder);

//Production Order
router.get("/productionorder", productionOrder.productionOrder);

//Supplier Order
router.get("/supplierOrder", supplierOrder.supplierOrder);

//Stamp
router.get("/stamp", stamp.stamp);

app.use("/", router);
app.use(express.static('public'));

app.use("*", function (req, res) {
    if (!config.notfoundpage) {
		res.redirect('/');
	} else {
        res.sendFile(reqPath + "404.html");
	}
});

app.listen(3000, function () {
    console.log("Live at Port 3000");
});
