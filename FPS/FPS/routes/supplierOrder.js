﻿'use strict';
var express = require('express');
var config = require('../server/config');
var path = require('path');
var reqPath = path.join(__dirname, '../views/supplierOrder/');
var jwt_simple = require('jwt-simple');

var authenticate = function (req) {
    if (req.session.jwt === undefined || req.session.jwt === null || req.session.jwt === '')
        return false;

    var decoded = jwt_simple.decode(req.session.jwt, config.secret);
    if (decoded.role.slug === 'admin')
        return true;

    return false;
};

exports.supplierOrder = function (req, res) {
    if (authenticate(req)) {
        res.sendFile(reqPath + "index.html");
        return;
    } else {
        return res.redirect('/');
    }
};
