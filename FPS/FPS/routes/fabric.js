﻿'use strict';
var express = require('express');
var config = require('../server/config');
var path = require('path');
var reqPath = path.join(__dirname, '../views/fabric/');
var jwt_simple = require('jwt-simple');

var authenticate = function (req) {
    if (req.session.jwt === undefined || req.session.jwt === null || req.session.jwt === '')
        return false;

    return true;
};

exports.fabric = function (req, res) {
    if (authenticate(req)) {
        res.sendFile(reqPath + "index.html");
        return;
    } else {
        return res.redirect('/');
    }
};
