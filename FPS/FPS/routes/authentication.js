﻿'use strict';
var express = require('express');
var path = require('path');
var reqPath = path.join(__dirname,'../views/login/');

var authenticate = function (req) {
    if (req.session.jwt === undefined || req.session.jwt === null || req.session.jwt === '')
        return false;

    return true;
};

exports.getAuth = function (req, res) {
    res.send(req.session.jwt);
    res.end();
};

exports.setAuth = function (req, res) {
    req.session.jwt = req.body.jwt;
    res.end();
};

exports.logout = function (req, res) {
    req.session.destroy();
    res.end();
};

exports.login = function (req, res) {
    if (!authenticate(req)) {
        res.sendFile(reqPath + "login.html");
        return;
    } else {
        return res.redirect('/');
    }
};

exports.signIn = function (req, res) {
    req.session.destroy();
    res.sendFile(reqPath + "signin.html");
    return;
};
